//
//  CoreForgotPasswordViewController.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/10/19.
//

import UIKit

open class CoreForgotPasswordViewController: UIViewController {
    public let forgotPasswordView: ForgotPasswordView = {
        let view = ForgotPasswordView()
        view.headerLabel.text = "Elige cualquiera de estas 3 formas para restablecer tu contraseña:"
        view.faqLabel.text = "Preguntas frecuentes"
        view.emailLabel.text = "Correo Electrónico"
        return view
    }()
    
    open override func loadView() {
        view = UIView()
        view.backgroundColor = .white
        view.addSubview(forgotPasswordView)
        forgotPasswordView.edgesToSuperview(usingSafeArea: true)
        forgotPasswordView.emailButton.addTarget(self, action: #selector(emailButtonTouchUpInside(_:)), for: .touchUpInside)
        forgotPasswordView.faqButton.addTarget(self, action: #selector(faqButtonTouchUpInside(_:)), for: .touchUpInside)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc open func emailButtonTouchUpInside(_ sender: UIButton) {
        
    }
    
    @objc open func faqButtonTouchUpInside(_ sender: UIButton) {
        
    }
}
