//
//  CoreLoginViewController.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 12/29/18.
//

import UIKit
import TinyConstraints

public protocol CoreLoginViewControllerDelegate: class, SideMenuShowable {
    func viewControllerDidLogInSuccessfully(_ viewController: CoreLoginViewController)
    func logInViewControllerDidSelectForgotPassword(_ viewController: CoreLoginViewController)
}

open class CoreLoginViewController: UIViewController {

    public let logInView: CoreLoginView = {
        let view = CoreLoginView()
        view.backgroundColor = .white
        return view
    }()
    
    public weak var delegate: CoreLoginViewControllerDelegate?
    
    open override func loadView() {
        view = logInView
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        logInView.didTapLogIn = { [weak self] (button, view) in
            guard let self = self else { return }
            self.delegate?.viewControllerDidLogInSuccessfully(self)
        }
        
        logInView.didTapForgotPassword = { [weak self] (button, view) in
            guard let self = self else { return }
            self.delegate?.logInViewControllerDidSelectForgotPassword(self)
        }
        
    }
    
}

extension CoreLoginViewController: StoryboardInitializable {
    public static var storyboardName: String {
        return "CoreLogin"
    }
}
