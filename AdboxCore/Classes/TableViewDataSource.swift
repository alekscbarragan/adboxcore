//
//  TableViewDataSource.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 12/28/18.
//

import UIKit

public class TableViewDataSource<T, Cell: UITableViewCell>: NSObject, UITableViewDataSource, UITableViewDelegate {
    public typealias ConfigureCell = (Cell, T) -> Void
    let items: [T]
    let cellIdentifier: String
    let configureCell: ConfigureCell
    
    public typealias DidSelectItemAt = (IndexPath, Cell, T, UITableView) -> Void
    private var didSelectItemAt: (DidSelectItemAt)?
    
    public init(withItems items: [T], cellIdentifier: String, configureCell: @escaping ConfigureCell) {
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.configureCell = configureCell
        super.init()
    }
    
    @available(*, unavailable)
    private override init() {
        fatalError("Init should not be used.")
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as? Cell else {
            return UITableViewCell()
        }
        
        let item = items[indexPath.row]
        configureCell(cell, item)
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    public func didSelectCellAtIndexPath(didSelectItemAt: @escaping (DidSelectItemAt)) {
        self.didSelectItemAt = didSelectItemAt
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? Cell else { return }
        let item = items[indexPath.row]
        didSelectItemAt?(indexPath, cell, item, tableView)
    }
}

public class CollectionViewDataSource<Cell: UICollectionViewCell, T>: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    public typealias ConfigureCell = (Cell, T) -> Void
    let items: [T]
    let cellIdentifier: String
    let configureCell: ConfigureCell
    
    private var didSelectItemAt: ((IndexPath, Cell, T, UICollectionView) -> Void)?
    
    public init(with items: [T], cellIdentifier: String, configureCell: @escaping ConfigureCell) {
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.configureCell = configureCell
        super.init()
    }
    
    @available(*, unavailable)
    private override init() {
        fatalError("Init should not be used.")
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? Cell else {
            return UICollectionViewCell()
        }
        
        let item = items[indexPath.row]
        configureCell(cell, item)
        return cell
    }
    
    // MARK: - Collection view delegate
    public func didSelectCellAtIndexPath(didSelectItemAt: @escaping ((IndexPath, Cell, T, UICollectionView) -> Void)) {
        self.didSelectItemAt = didSelectItemAt
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? Cell else { return }
        didSelectItemAt?(indexPath, cell, items[indexPath.row], collectionView)
    }
    
}

