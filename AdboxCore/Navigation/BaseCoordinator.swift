//
//  BaseCoordinator.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/2/19.
//

import Foundation
import UIKit

public protocol BaseCoordinator: class {
    var navigationController: UINavigationController { get }
    func start()
}
