//
//  SideMenuShowable.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/2/19.
//

import Foundation

public protocol SideMenuShowable {
    func showSideMenu()
}
