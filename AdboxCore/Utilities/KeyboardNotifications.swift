//
//  KeyboardNotifications.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 2/11/19.
//

import UIKit

public struct SystemNotification {
    public static let keyboardShowNotification = NotificationDescriptor<KeyboardData>(name: UIResponder.keyboardWillShowNotification, convert: KeyboardData.init)
    public static let keyboardHideNotification = NotificationDescriptor<KeyboardData>(name: UIResponder.keyboardWillHideNotification, convert: KeyboardData.init)
}

public struct NotificationDescriptor<A> {
    /// Name of the notification
    public let name: Foundation.Notification.Name

    /// Method to convert notification.userInfo to desired type
    public let convert: (Foundation.Notification) -> A
}

public class Token {
    let center: NotificationCenter
    let token: NSObjectProtocol

    public init(token: NSObjectProtocol, center: NotificationCenter) {
        self.token = token
        self.center = center
    }

    deinit {
        center.removeObserver(token)
    }
}

public extension NotificationCenter {
    public func addObserver<A>(forDescriptor d: NotificationDescriptor<A>, using block: @escaping (A) -> Void) -> Token {
        let t = addObserver(forName: d.name, object: nil, queue: nil, using: { notification in
            block(d.convert(notification))
        })
        return Token(token: t, center: self)
    }
}

public struct KeyboardData {
    let beginFrame: CGRect
    let endFrame: CGRect
    let curve: UIView.AnimationCurve? //If we want to use it to synchronize our animation
    let duration: TimeInterval
}

public extension KeyboardData {
    init(notification: Foundation.Notification) {
        guard let userInfo = notification.userInfo else {
            self.beginFrame = CGRect.zero
            self.endFrame = CGRect.zero
            self.curve = nil
            self.duration = 0

            return
        }

        self.beginFrame = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect ?? CGRect.zero
        self.endFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect ?? CGRect.zero
        self.curve = UIView.AnimationCurve(rawValue: (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int) ?? 0)
        self.duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval ?? 0
    }
}

public protocol Keyboard: class {
    var isKeyboardAware: Bool { get set }
    var keyboardShowToken: Token? { get set }
    var keyboardHideToken: Token? { get set }
    var scrollView: UIScrollView { get }
    func updateKeyboardObserverStatus()
}

public extension Keyboard where Self: Keyboard, Self: UIViewController {
    public func updateKeyboardObserverStatus() {
        if isKeyboardAware {
            keyboardShowToken = NotificationCenter.default.addObserver(forDescriptor: SystemNotification.keyboardShowNotification) { keyboardData in

                let frameIntersection = self.view.frame.intersection(keyboardData.endFrame)
                if !frameIntersection.isNull {
                    self.scrollView.contentInset.bottom = frameIntersection.height
                }
            }

            keyboardHideToken = NotificationCenter.default.addObserver(forDescriptor: SystemNotification.keyboardHideNotification) { _ in
                self.scrollView.contentInset.bottom = 0
            }

        } else {
            keyboardShowToken = nil
            keyboardHideToken = nil
        }
    }
}
