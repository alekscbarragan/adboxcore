//
//  NibLoadable.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 12/30/18.
//

import Foundation
import UIKit

public protocol NibLoadable: TypedName {
    
    /// Loads view from nib file using the nib name provided.
    ///
    /// - Parameter nibName: File name of the nib file.
    /// - Returns: Custom view from nib.
    func load(nibName: String) -> UIView
    
    /// Loads the view from a Nib file using the same class name as nib name.
    ///
    /// - Returns: Custom view from nib.
    func load() -> UIView
}

public extension NibLoadable where Self: UIView {
    @discardableResult public func load(nibName: String) -> UIView {
//        let bundle = Bundle(for: type(of: self))
        let bundle = Bundle(for: Self.self)
        print(bundle)
        let nib = UINib(nibName: nibName, bundle: bundle)
        // swiftlint:disable:next force_cast
        guard let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView else {
            fatalError()
        }
        view.frame = bounds
        return view
    }
    
    @discardableResult public func load() -> UIView {
        let view = load(nibName: Self.typedName)
        addSubview(view)
        return view
    }
    
    @discardableResult public func load(with bundle: Bundle, nibName: String) -> UIView {
        let nib = UINib(nibName: nibName, bundle: bundle)
        // swiftlint:disable:next force_cast
        guard let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView else {
            fatalError()
        }
        view.frame = bounds
        return view
    }
}
