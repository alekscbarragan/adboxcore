//
//  StoryboardInitializable.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 12/29/18.
//

import Foundation
import UIKit

public protocol StoryboardInitializable {
    static var storyboardName: String { get }
    static var storyboardBundle: Bundle? { get }
    static var storyboardIdentifier: String { get }
    static func makeFromStoryboard() -> Self
}

public extension StoryboardInitializable where Self: UIViewController {
    
    static var storyboardBundle: Bundle? {
        return Bundle(for: Self.self)
    }
    
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
    
    static func makeFromStoryboard() -> Self {
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: storyboardBundle)
        
        guard let viewController = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as? Self else {
            fatalError("No view controller with identifier \(storyboardIdentifier) in storyboard")
        }
        
        return viewController
    }
}
