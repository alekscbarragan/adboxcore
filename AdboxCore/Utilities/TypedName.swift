//
//  TypedName.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 12/30/18.
//

import Foundation

public protocol TypedName: AnyObject {
    static var typedName: String { get }
}

// Swift Objects
public extension TypedName {
    static var typedName: String {
        let type = String(describing: self)
        return type
    }
}

// Bridge to Obj-C
extension NSObject: TypedName {
    public class var typedName: String {
        let type = String(describing: self)
        return type
    }
}
