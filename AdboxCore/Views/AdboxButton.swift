//
//  AdboxButton.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 2/6/19.
//

import UIKit

/// Adbox Custom Button
public class AdboxButton: UIButton {

    public enum Axis: Int {
        case horizontal
        case vertical
    }

    public var axis: Axis = .horizontal

    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    public var onTap: (() -> Void)?
    
    public var gradientColors: [CGColor] = [] {
        didSet {
            guard let gradientLayer = layer as? CAGradientLayer else { return }
            if axis == .horizontal {
                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            } else {
                gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
                gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
            }

            gradientLayer.colors = gradientColors
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
    private func setUp() {
        addTarget(self, action: #selector(didTap(sender:)), for: .touchUpInside)
    }
    
    open func underlinedText(text: String, textColor: UIColor = .white, fontSize: CGFloat = 15) {
        let attributes: [NSAttributedString.Key: Any] = [
            .font : UIFont.boldSystemFont(ofSize: fontSize),
            .foregroundColor : textColor,
            .underlineStyle : NSUnderlineStyle.single.rawValue]
        
        let attributedString = NSAttributedString(string: text, attributes: attributes)
        setAttributedTitle(attributedString, for: .normal)
    }
    
    @objc func didTap(sender: AdboxButton) {
        onTap?()
    }

}

public class AdboxLabel: UILabel, AdboxGradientable {
    public var axis: Axis = .horizontal
    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
}

public enum Axis: Int {
    case horizontal
    case vertical
}

public protocol AdboxGradientable {
    var gradientColors: [CGColor] { set get }
    var axis: Axis { set get }
}

extension AdboxGradientable where Self: UIView {
    public var gradientColors: [CGColor] {
        
        get {
            guard let gradientLayer = layer as? CAGradientLayer else { return [] }
            return gradientLayer.colors as? [CGColor] ?? []
        }
        
        set {
            guard let gradientLayer = layer as? CAGradientLayer else { return }
            if axis == .horizontal {
                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            } else {
                gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
                gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
            }
            
            gradientLayer.colors = newValue
        }
    }
}
