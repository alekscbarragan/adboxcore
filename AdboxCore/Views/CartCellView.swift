//
//  CartCellView.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/7/19.
//

import Foundation
import UIKit
import TinyConstraints

public protocol CartCell {
    var productImageView: UIImageView { get }
    var stackView: UIStackView { get }
    var minusButton: UIButton { get }
    var plusButton: UIButton { get }
    var deleteButton: UIButton { get }
    var categoryLabel: UILabel { get }
    var skuLabel: UILabel { get }
    var costLabel: UILabel { get }
}

open class CartCellView: UIView, CartCell {
    public var productImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .blue
        return imageView
    }()
    
    public var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 4.0
        return stackView
    }()
    
    public var minusButton: UIButton = {
        let button = Template.Button(withTitle: "-")
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .blue
        return button
    }()
    
    
    public var plusButton: UIButton = {
        let button = Template.Button(withTitle: "+")
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .blue
        return button
    }()
    
    public var deleteButton: UIButton = {
        let button = Template.Button(withTitle: "Remove")
        return button
    }()
    
    public var quantityLabel: UILabel = {
        let label = Template.Label(with: " 1 ")
        return label
    }()
    
    public var categoryLabel: UILabel = {
        let label = Template.Label(with: "Premio: ")
        return label
    }()
    
    public var skuLabel: UILabel = {
        let label = Template.Label(with: "SKU: ")
        return label
    }()
    
    public var costLabel: UILabel = {
        let label = Template.Label(with: "XXX Puntos")
        return label
    }()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupConstraints() {
        [productImageView, stackView, deleteButton].forEach { addSubview($0) }
        productImageView.edgesToSuperview(excluding: .right)
        productImageView.right(to: stackView, stackView.leftAnchor, offset: -16)
        productImageView.height(110)
        productImageView.widthToHeight(of: productImageView)
        
        [categoryLabel, skuLabel, costLabel].forEach { stackView.addArrangedSubview($0) }
        stackView.edgesToSuperview(excluding: [.left, .bottom])
        
        let buttonsStackView = UIStackView(arrangedSubviews: [minusButton, quantityLabel, plusButton])
        addSubview(buttonsStackView)
        buttonsStackView.axis = .horizontal
        buttonsStackView.distribution = .fillProportionally
        buttonsStackView.bottom(to: productImageView, productImageView.bottomAnchor)
        buttonsStackView.left(to: productImageView, productImageView.rightAnchor, offset: 16)
        
        deleteButton.edgesToSuperview(excluding: [.top, .left, .bottom])
        deleteButton.centerY(to: buttonsStackView)
    }
    
}

public typealias Text = String
public enum Template {
    public static func Button(withTitle title: String = "") -> UIButton {
        let button = UIButton(title: title)
        return button
    }
    
    public static func Label(with text: Text = "") -> UILabel {
        let label = UILabel()
        label.text = text
        label.textColor = .black
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.minimumScaleFactor = 0.5
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        return label
    }
    
    public static func createAdboxLabel(text: String = "") -> AdboxLabel {
        let label = AdboxLabel()
        label.text = text
        label.textColor = .black
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.minimumScaleFactor = 0.5
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        return label
    }
    
    public static func StackView(axis: NSLayoutConstraint.Axis, distribution: UIStackView.Distribution) -> UIStackView {
        let stackView = UIStackView()
        stackView.distribution = distribution
        stackView.axis = axis
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
}

public extension UIButton {
    convenience init(title: String) {
        self.init()
        setTitle(title, for: .normal)
        setTitleColor(.black, for: .normal)
        titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
    }
}

public extension UIStackView {
    public func addArrangedSubviews(views: UIView...) {
        views.forEach { addArrangedSubview($0) }
    }
}
