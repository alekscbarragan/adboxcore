//
//  CatalogCellView.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 12/31/18.
//

import Foundation
import UIKit
import TinyConstraints

public protocol CatalogCell {
    var categoryImageView: UIImageView { get }
    var categoryLabel: UILabel { get }
    var footerLabel: UILabel { get }
    var footerView: GradientView { get }
}

open class CatalogCellView: UIView, CatalogCell {
    public let categoryImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .purple
        return imageView
    }()
    
    public let categoryLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    public let footerLabel: UILabel = {
        let label = UILabel()
        label.text = "VER"
        label.font = UIFont.boldSystemFont(ofSize: 17)
        return label
    }()
    
    public let footerView: GradientView = {
        let view = GradientView()
        view.backgroundColor = .blue
        return view
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupConstraints() {
        [categoryImageView, categoryLabel, footerView, footerLabel].forEach { addSubview($0) }
        footerView.edgesToSuperview(excluding: .top)
        footerView.heightToSuperview(multiplier: 0.2)
        categoryImageView.edgesToSuperview(excluding: .bottom)
        categoryImageView.bottom(to: footerView, footerView.topAnchor)
        categoryLabel.edgesToSuperview(excluding: [.top, .bottom], insets: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20))
        categoryLabel.bottom(to: footerView, footerView.topAnchor, offset: -8)
        footerLabel.rightToSuperview(offset: -20)
        footerLabel.centerY(to: footerView)
    }
    
    open func categoryTitleTextAlignment(_ alignment: NSTextAlignment) {
        categoryLabel.textAlignment = alignment
    }
    
}
