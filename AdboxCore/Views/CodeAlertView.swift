//
//  CodeAlertView.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/7/19.
//

import Foundation
import UIKit
import TinyConstraints

public protocol CodeAlert {
    var titleLabel: UILabel { get }
    var headerLabel: UILabel { get }
    var imageView: UIImageView { get }
    var subtitleLabel: UILabel { get }
    var codeLabel: UILabel { get }
    var acceptButton: UIButton { get }
}

open class CodeAlertView: UIView, CodeAlert {
    public var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .lightGray
        return imageView
    }()
    
    public var titleLabel: UILabel = {
        let label = Template.Label(with: "Title")
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        label.textAlignment = .center
        return label
    }()
    
    public var headerLabel: UILabel = {
        let label = Template.Label(with: "Header")
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .blue
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    public var subtitleLabel: UILabel = {
        let label = Template.Label(with: "Subtitle")
        label.textAlignment = .center
        return label
    }()
    
    public var codeLabel: UILabel = {
        let label = Template.Label(with: "Code")
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        return label
    }()
    
    public let acceptButton: UIButton = {
        let button = Template.Button(withTitle: "ACEPTAR")
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        return button
    }()
    
    public let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    public let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    public var headerTintColor: UIColor = .blue {
        didSet { headerTintColodDidChange() }
    }
    
    public var headerTextColor: UIColor = .white {
        didSet { headerTextColorDidChange() }
    }
    
    public convenience init(header: String, title: String, subtitle: String, code: String) {
        self.init(frame: .zero)
        headerLabel.text = header
        titleLabel.text = title
        subtitleLabel.text = subtitle
        codeLabel.text = code
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        setupConstraints()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func tintColorDidChange() {
        super.tintColorDidChange()
        print(#function)
        [titleLabel, subtitleLabel, codeLabel].forEach { $0.textColor = tintColor }
        acceptButton.setTitleColor(tintColor, for: .normal)
    }
    
    open func headerTintColodDidChange() {
        headerLabel.backgroundColor = headerTintColor
    }
    
    open func headerTextColorDidChange() {
        headerLabel.textColor = headerTextColor
    }
    
    open func setupConstraints() {
        addSubview(containerView)
        containerView.edgesToSuperview(insets: UIEdgeInsets(top: 80, left: 20, bottom: 80, right: 20))
        [headerLabel, contentView].forEach { containerView.addSubview($0) }
        headerLabel.height(50)
        headerLabel.edgesToSuperview(excluding: .bottom)
        headerLabel.bottom(to: contentView, contentView.topAnchor)
        contentView.edgesToSuperview(excluding: .top)
        [imageView, titleLabel, subtitleLabel, codeLabel, acceptButton].forEach { contentView.addSubview($0) }
        imageView.height(150)
        imageView.width(150)
        imageView.layer.cornerRadius = 75
        imageView.clipsToBounds = true
        imageView.center(in: contentView)
        titleLabel.edgesToSuperview(excluding: .bottom)
        titleLabel.bottom(to: imageView, imageView.topAnchor, offset: -16)
        subtitleLabel.top(to: imageView, imageView.bottomAnchor, offset: 24)
        subtitleLabel.edgesToSuperview(excluding: [.top, .bottom])
        codeLabel.centerX(to: contentView)
        codeLabel.top(to: subtitleLabel, subtitleLabel.bottomAnchor, offset: 16)
        acceptButton.edgesToSuperview(excluding: [.left, .top], insets: UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 20))
    }
}
