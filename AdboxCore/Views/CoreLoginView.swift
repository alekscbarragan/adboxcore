//
//  CoreLoginView.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 12/29/18.
//

import UIKit
import Foundation
import AloeStackView

public protocol BaseLogInView {
    var backgroundImageView: UIImageView { get }
    var imageView: UIImageView { get }
    var usernameLabel: UILabel { get }
    var passwordLabel: UILabel { get }
    var usernameTextField: UITextField { get }
    var passwordTextField: UITextField { get }
    var logInButton: AdboxButton { get }
    var forgotButton: UIButton { get }
}

open class CoreLoginView: UIView, BaseLogInView {
    public let backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    public let imageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "Placeholder"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    public let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "Username"
        label.textAlignment = .center
        label.textColor = .black
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.contentMode = .bottom
       return label
    }()
    
    public let passwordLabel: UILabel  = {
        let label = UILabel()
        label.text = "Password"
        label.textAlignment = .center
        label.textColor = .black
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    public let usernameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Usuario"
        textField.borderStyle = .roundedRect
        textField.textAlignment = .center
        textField.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
        return textField
    }()
    
    public let passwordTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Contraseña"
        textField.borderStyle = .roundedRect
        textField.textColor = .black
        textField.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
        textField.textAlignment = .center
        return textField
    }()
    
    public let logInButton: AdboxButton = {
        let button = AdboxButton(type: .custom)
        button.setTitle("Iniciar", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .lightGray
       return button
    }()
    
    public let forgotButton: UIButton = {
        let button = UIButton()
        button.setTitle("¿Olvidaste tu contraseña?", for: .normal)
        button.setTitleColor(.black, for: .normal)
        return button
    }()

    public typealias ButtonConfigurationCompletion = ((UIButton) -> Void)
    public typealias TextFieldConfigurationCompletion = ((UITextField) -> Void)
    
    public var didTapLogIn: ((UIButton, CoreLoginView) -> Void)?
    public var didTapForgotPassword: ((UIButton, CoreLoginView) -> Void)?
    public var configureLogInButton: ((AdboxButton) -> Void)? {
        didSet { configureLogInButton?(logInButton) }
    }
    
    public var configureUserLabel: ((UILabel) -> Void)? {
        didSet { configureUserLabel?(usernameLabel) }
    }
    
    public var configurePasswordLabel: ((UILabel) -> Void)? {
        didSet { configurePasswordLabel?(passwordLabel) }
    }
    
    public var configureForgotPasswordButton: ((UIButton) -> Void)? {
        didSet { configureForgotPasswordButton?(forgotButton) }
    }

    public var configurePasswordTextField: TextFieldConfigurationCompletion? {
        didSet { configurePasswordTextField?(passwordTextField) }
    }

    public func configureUsernameTextField(completion: @escaping TextFieldConfigurationCompletion) {
        completion(usernameTextField)
    }

    public func passwordTextFieldConfiguration(completion: @escaping TextFieldConfigurationCompletion) {
        self.configurePasswordTextField = completion
    }

    public func didTapLogInButton(completion: @escaping (UIButton, CoreLoginView) -> Void) {
        didTapLogIn = completion
    }
    
    convenience public init() {
        self.init(frame: .zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: .zero)
        setupSubconstraints()
        setupButtonActions()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public let stack = AloeStackView()
    open func setupSubconstraints() {
        [backgroundImageView, imageView, stack].forEach { addSubview($0) }
        backgroundImageView.edgesToSuperview()
        imageView.edgesToSuperview(excluding: .bottom, insets: UIEdgeInsets(top: 20, left: 40, bottom: 0, right: 40), usingSafeArea: true)
        imageView.bottom(to: stack, stack.topAnchor, offset: -40)
        imageView.heightToSuperview(multiplier: 0.25)
        stack.rowInset = UIEdgeInsets(top: 4, left: 40, bottom: 4, right: 40)
        stack.separatorInset = .zero
        stack.hidesSeparatorsByDefault = true
        stack.addRows([usernameLabel, usernameTextField, passwordLabel, passwordTextField, logInButton, forgotButton])
        stack.edgesToSuperview(excluding: .top)
        stack.backgroundColor = .clear

        let standardHeight: CGFloat = 40
        var intrinsicHeight: CGFloat = 0
        let constantHeights: [UIView: CGFloat] = [logInButton: standardHeight,
                                                  usernameTextField: standardHeight,
                                                  passwordTextField: standardHeight]

        stack.getAllRows().forEach { view in
            if let height = constantHeights[view] {
                view.height(height)
                intrinsicHeight += height
            }
        }
        
        let buttonsVerticalSpace: CGFloat = 40
        stack.setInset(forRow: logInButton, inset: UIEdgeInsets(top: buttonsVerticalSpace, left: 80, bottom: 0, right: 80))

    }
    
    public func setupButtonActions() {
        logInButton.addTarget(self, action: #selector(logInTouchUpInside(_:)), for: .touchUpInside)
        forgotButton.addTarget(self, action: #selector(forgotPasswordTouchUpInside(_:)), for: .touchUpInside)
    }
    
    @objc private func logInTouchUpInside(_ sender: UIButton) {
        didTapLogIn?(sender, self)
    }
    
    @objc private func forgotPasswordTouchUpInside(_ sender: UIButton) {
        didTapForgotPassword?(sender, self)
    }
    
}
