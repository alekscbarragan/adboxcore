//
//  ExchangeDetailCell.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/7/19.
//

import Foundation
import TinyConstraints

public protocol ExchangeDetailCell {
    var imageView: UIImageView { get }
    var titleLabel: UILabel { get }
    var subtitleLabel: UILabel { get }
}

open class ExchangeDetailCellView: UIView, ExchangeDetailCell {
    public var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .lightGray
        return imageView
    }()
    
    public var titleLabel: UILabel = {
        let label = Template.Label(with: "Title")
        label.font = UIFont.boldSystemFont(ofSize: 15)
        return label
    }()
    
    public var subtitleLabel: UILabel = {
        let label = Template.Label(with: "Subtitle")
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    public var stackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    public var textColor: UIColor = .black {
        didSet {
            titleLabel.textColor = textColor
            subtitleLabel.textColor = textColor
            imageView.backgroundColor = textColor
        }
    }
    
    public convenience init(title: String, subtitle: String) {
        self.init(frame: .zero)
        titleLabel.text = title
        subtitleLabel.text = subtitle
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    @available(*, unavailable)
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupConstraints() {
        [titleLabel, subtitleLabel].forEach { stackView.addArrangedSubview($0) }
        [imageView, stackView].forEach { addSubview($0) }
        imageView.height(50)
        imageView.width(50)
        imageView.layer.cornerRadius = 25
        imageView.clipsToBounds = true
        imageView.edgesToSuperview(excluding: .right, insets: UIEdgeInsets(top: 4, left: 10, bottom: 4, right: 10))
        stackView.left(to: imageView, imageView.rightAnchor, offset: 16)
        stackView.edgesToSuperview(excluding: [.left, .bottom, .top])
        stackView.centerY(to: imageView)
    }
    
}
