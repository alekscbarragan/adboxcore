//
//  ForgotPasswordView.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/8/19.
//

import UIKit
public protocol ForgotPassword {
    var headerLabel: UILabel { get }
    var stackView: UIStackView { get }
    var faqStackView: UIStackView { get }
    var faqLabel: UILabel { get }
    var faqButton: UIButton { get }
    var emailLabel: UILabel { get }
    var emailButton: UIButton { get }
    var emailStackView: UIStackView { get }
}

open class ForgotPasswordView: UIView, ForgotPassword {
    public let headerLabel: UILabel = {
        let label = Template.Label(with: "Header")
        label.textAlignment = .center
        return label
    }()
    
    public var stackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    public var faqStackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .vertical
        stackView.distribution = .fill
        return stackView
    }()
    
    public var emailStackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .vertical
        stackView.distribution = .fill
        return stackView
    }()
    
    public var faqLabel: UILabel = {
        let label = Template.Label()
        label.textAlignment = .center
        return label
    }()
    
    public var emailLabel: UILabel = {
        let label = Template.Label()
        label.textAlignment = .center
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return label
    }()
    
    public var emailView: UIView = {
        let view = UIView()
        view.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return view
    }()
    
    public var emailButton: UIButton = {
        let button = Template.Button(withTitle: "Email Button")
        return button
    }()
    
    public var faqButton: UIButton = {
        let button = Template.Button(withTitle: "FAQ Button")
        return button
    }()
    
    public var headerContentView: UIView = {
        let view = UIView()
        return view
    }()
    
    public var headerBackgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    public var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    public var configureHeaderLabel: ((UILabel) -> Void)? {
        didSet {
            configureHeaderLabel?(headerLabel)
        }
    }
    
    public var configureFAQLabel: ((UILabel) -> Void)? {
        didSet {
            configureFAQLabel?(faqLabel)
        }
    }
    
    public var configureEmailLabel: ((UILabel) -> Void)? {
        didSet {
            configureEmailLabel?(emailLabel)
        }
    }
    
    public var configureHeaderBackground: ((_ background: UIImageView) -> Void)? {
        didSet {
            configureHeaderBackground?(headerBackgroundImageView)
        }
    }
    
    public var configureEmailButton: ((UIButton) -> Void)? {
        didSet {
            configureEmailButton?(emailButton)
        }
    }
    
    public var configureFaqButton: ((UIButton) -> Void)? {
        didSet {
            configureFaqButton?(faqButton)
        }
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupConstraints() {
        [headerContentView, contentView].forEach { addSubview($0) }
        
        // Header content view
        headerContentView.addSubviews(headerBackgroundImageView, headerLabel, autolayout: true)
        headerBackgroundImageView.edgesToSuperview()
        headerLabel.centerInSuperview()
        headerLabel.edgesToSuperview(excluding: [.top, .bottom])
        headerContentView.heightToSuperview(multiplier: 0.25)
        headerContentView.edgesToSuperview(excluding: .bottom)
        
        // Content view
        [faqLabel, faqButton].forEach { faqStackView.addArrangedSubview($0) }
        [emailLabel, emailButton].forEach { emailStackView.addArrangedSubview($0) }
        [faqStackView, emailStackView].forEach { stackView.addArrangedSubview($0) }
        
        contentView.edgesToSuperview(excluding: .top)
        contentView.top(to: headerContentView, headerLabel.bottomAnchor, offset: 40)
        
        contentView.addSubviews(stackView, autolayout: true)
        stackView.leftToSuperview()
        stackView.rightToSuperview()
        stackView.centerInSuperview()
        
        stackView.setCustomSpacing(40, after: faqStackView)
        faqStackView.setCustomSpacing(16, after: faqLabel)
        emailStackView.setCustomSpacing(16, after: emailLabel)
    }
    
}

extension UIView {
    convenience init(with backgroundColor: UIColor) {
        self.init(frame: .zero)
        self.backgroundColor = backgroundColor
    }
    
    static var flexView: UIView {
        return UIView(with: .purple)
    }
}

