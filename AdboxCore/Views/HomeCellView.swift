//
//  HomeTableViewCell.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 12/30/18.
//

import UIKit
import TinyConstraints

protocol HomeCell {
    var titleLabel: UILabel { get }
    var optionImageView: UIImageView { get }
}

open class HomeCellView: UIView, HomeCell, Shadowable {
    public let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Catálogo de premios"
        label.numberOfLines = 0
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    public let optionImageView: UIImageView = {
        return UIImageView()
    }()
    
    public let placeholderView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    /// It will only be called once inmmediatly after being set.
    public var configureTitleLabel: ((UILabel) -> Void)? {
        didSet {
            configureTitleLabel?(titleLabel)
        }
    }
    
    /// It will only be called once inmmediatly after being set.
    public var configureOptionImageView: ((UIImageView) -> Void)? {
        didSet {
            configureOptionImageView?(optionImageView)
        }
    }
    
    /// It will only be called once inmmediatly after being set.
    public var configure: ((HomeCellView) -> Void)? {
        didSet {
            configure?(self)
        }
    }
    
    // MARK: - Inits
    convenience public init() {
        self.init(frame: .zero)
    }
    
    convenience public init(with title: String, image: UIImage) {
        self.init(frame: .zero)
        titleLabel.text = title
        optionImageView.image = image
    }
    
    convenience public init(with title: String, urlString: String) {
        self.init(frame: .zero)
        titleLabel.text = title
        // TODO: Create URL and fetch it to `optionImageView`
    }
    
    convenience public init(title: String = "", imageNamed: String) {
        self.init(frame: .zero)
        titleLabel.text = title
        optionImageView.image = UIImage(named: imageNamed)
    }
    
    public convenience init(title: String) {
        self.init(frame: .zero)
        titleLabel.text = title
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        applyShadow()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Open methods
    open func setupConstraints() {
        [optionImageView, titleLabel, placeholderView].forEach { addSubview($0) }
        
        titleLabel.right(to: placeholderView, placeholderView.leftAnchor, offset: -16)
        titleLabel.edgesToSuperview(excluding: .right,
                                    insets: UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 0))
        
        placeholderView.widthToHeight(of: placeholderView)
        placeholderView.edgesToSuperview(excluding: .left,
                                         insets: UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 16))
        optionImageView.edgesToSuperview()
    }
}

public protocol Shadowable {
    func applyShadow()
}

public extension Shadowable where Self: UIView {
    func applyShadow() {
        layer.cornerRadius = 3.0
        backgroundColor = .white
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 1, height: 3)
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 3
    }
}
