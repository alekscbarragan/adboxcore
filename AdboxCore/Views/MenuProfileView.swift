//
//  MenuProfileView.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/12/19.
//

import Foundation
import UIKit
import TinyConstraints
import AloeStackView

public protocol MenuProfile {
    var profileImageView: UIImageView { get }
    var logoImageView: UIImageView { get }
    var backgroundImageView: UIImageView { get }
    var settingsButton: UIButton { get }
    var pointsLabel: UILabel { get }
    
}

open class MenuProfileView: UIView, MenuProfile, Constrainable {
    public var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .blue
        imageView.clipsToBounds = true
        return imageView
    }()
    
    public var logoImageView: UIImageView = {
        let imageView = UIImageView()
        //        imageView.backgroundColor = .blue
        return imageView
    }()
    
    public var backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    public var settingsButton: UIButton = {
        let button = Template.Button(withTitle: "Ajutes de mi perfil")
        button.contentHorizontalAlignment = .left
        return button
    }()
    
    public var pointsLabel: UILabel = {
        let label = Template.Label(with: "PUNTOS: 12")
        label.textAlignment = .right
        return label
    }()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupConstraints() {
        let horizontalPadding: CGFloat = 16
        addSubviews(backgroundImageView, profileImageView, logoImageView, settingsButton, pointsLabel, autolayout: true)
        
        // Paddings
        let insets = TinyEdgeInsets(top: 40, left: horizontalPadding, bottom: 0, right: 0)
        var horizontalInsets = TinyEdgeInsets(top: 0, left: horizontalPadding, bottom: 0, right: horizontalPadding)
        
        // Background
        backgroundImageView.edgesToSuperview()
        
        // Profile image view
        profileImageView.height(100)
        profileImageView.widthToHeight(of: profileImageView)
        profileImageView.edgesToSuperview(excluding: [.right, .bottom], insets: insets)
        profileImageView.bottom(to: settingsButton, settingsButton.topAnchor, offset: -8)
        
        // Settings button
        settingsButton.edgesToSuperview(excluding: [.top, .bottom], insets: horizontalInsets)
        settingsButton.setContentHuggingPriority(.defaultHigh, for: .vertical)
        settingsButton.bottom(to: pointsLabel, pointsLabel.topAnchor)
        
        // Points label
        horizontalInsets.bottom = 8
        pointsLabel.edgesToSuperview(excluding: .top, insets: horizontalInsets)
        pointsLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        
        // Logo image view
        //        logoImageView.widthToSuperview(multiplier: 0.25)
        logoImageView.rightToSuperview(offset: -16)
        logoImageView.centerY(to: profileImageView)
        logoImageView.height(to: profileImageView)
        logoImageView.width(to: profileImageView)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2.0
    }
    
}

public protocol Constrainable {
    /// Setup your constraints withtin this method. If you override it, do not call `super`.
    func setupConstraints()
}

public extension UIView {
    public func addSubviews(_ views: UIView..., autolayout: Bool = true) {
        views.forEach {
            $0.translatesAutoresizingMaskIntoConstraints = !autolayout
            addSubview($0)
        }
    }
}

open class CoreSideMenuView: UIView, Constrainable {
    
    public let contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    public let backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .blue
        return imageView
    }()
    
    public var menuProfileView: MenuProfileView = {
        let view = MenuProfileView()
        return view
    }()
    
    public var stackView: AloeStackView = {
        let stackView = AloeStackView()
        return stackView
    }()
    
    public convenience init() {
        self.init(frame: .zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        setupStackView()
        setupView()
        backgroundColor = UIColor.black.withAlphaComponent(0.4)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapRootView(_:)))
        addGestureRecognizer(tapGesture)
    }
    
    @objc
    open func didTapRootView(_ sender: UITapGestureRecognizer) {
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupConstraints() {
        addSubview(contentView)
        contentView.edgesToSuperview(excluding: .right)
        contentView.widthToSuperview(multiplier: 0.75)
        contentView.addSubviews(backgroundImageView, menuProfileView, stackView, autolayout: true)
        backgroundImageView.edgesToSuperview()
                menuProfileView.heightToSuperview(multiplier: 0.25)
        menuProfileView.edgesToSuperview(excluding: .bottom)
        menuProfileView.bottom(to: stackView, stackView.topAnchor)
        stackView.edgesToSuperview(excluding: .top)
        stackView.backgroundColor = .clear
    }
    
    /// Override this method to set your `AloeStackView` cells.
    /// If you don't subclass, you may use the method `addRow(:withTapHandler:)`
    open func setupStackView() { }
    
    // Override this method if you want to prepare/configure the view when view is being created.
    open func setupView() { }
    
    open func addRow(_ row: UIView, withTapHandler handler: ((UIView) -> Void)?) {
        stackView.addRow(row)
        if let handler = handler {
            row.isUserInteractionEnabled = true
            stackView.setTapHandler(forRow: row, handler: handler)
        }
    }
}

open class MyPointsView: UIView, Constrainable {
    
    public let stackView: AloeStackView = {
        let stackView = AloeStackView()
        return stackView
    }()
    
    public let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        return tableView
    }()
    
    public var button: UIButton = {
        let button = Template.Button(withTitle: "Historial de Canje")
        button.backgroundColor = .blue
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    public let wonPointsCellView: ExchangeDetailCellView = {
        let view = ExchangeDetailCellView(title: "Puntos Ganados", subtitle: "41,678")
        return view
    }()
    
    public let exchangedPointsCellView: ExchangeDetailCellView = {
        let view = ExchangeDetailCellView(title: "Puntos Canjeados", subtitle: "20,678")
        return view
    }()
    
    public let availablePointsCellView: ExchangeDetailCellView = {
        let view = ExchangeDetailCellView(title: "Puntos Disponibles", subtitle: "9,999")
        return view
    }()
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupConstraints() {
        addSubviews(tableView, button)
        stackView.rowInset.top = 8
        stackView.hidesSeparatorsByDefault = true
//        stackView.separatorHeight = 0
        stackView.addRows([wonPointsCellView, exchangedPointsCellView, availablePointsCellView])
        tableView.edgesToSuperview(excluding: .bottom)
        tableView.bottom(to: button, button.topAnchor, offset: -16)
        button.edgesToSuperview(excluding: .top)
        button.height(40)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        if tableView.tableHeaderView == nil {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 250))
            view.backgroundColor = .blue
            view.addSubview(stackView)
            tableView.tableHeaderView = view
            stackView.edgesToSuperview()
        }
    }
    
}
