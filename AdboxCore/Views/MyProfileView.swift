//
//  MyProfileView.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/27/19.
//

import UIKit
import AloeStackView
import TinyConstraints

open class MyProfileView: UIView, Constrainable {
    
    var elements: [String] = []
    lazy var defaultElements = ["*Calle", "*No. Exterior", "No. Interior", "*C.P.", "*Colonia", "*Ciudad", "*Municipio", "*Estado"]
    lazy var views: [UIView] = []
    let stackView: AloeStackView = {
        let stackView = AloeStackView()
        stackView.hidesSeparatorsByDefault = true
        stackView.rowInset = UIEdgeInsets(top: 4, left: stackView.rowInset.left, bottom: 4, right: stackView.rowInset.right)
        return stackView
    }()
    
    var borderColor: UIColor = .clear {
        didSet {
            setBorderColor(with: borderColor)
        }
    }
    
    let privacySwitch: UISwitch = {
        let privacySwitch = UISwitch()
        return privacySwitch
    }()
    
    let termsAndConditionsSwitch: UISwitch = {
        let termsAndConditionsSwitch = UISwitch()
        return termsAndConditionsSwitch
    }()
    
    let privacyLabel = Template.Label(with: "Acepto el aviso de privacidad")
    let termsAndConditionsLabel = Template.Label(with: "Acepto los términos y condiciones")
    
    let saveButton: UIButton = {
        let button = UIButton()
        button.setTitle("GUARDAR", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
        button.setBackgroundImage(UIImage(named: "app-nav-background"), for: .normal)
        return button
    }()
    
    public init(elements: [String] = []) {
        super.init(frame: .zero)
        if elements.isEmpty {
            self.elements = defaultElements
        } else {
            self.elements = elements
        }
        setupConstraints()
    }
    
    public init(views: [UIView] = []) {
        super.init(frame: .zero)
        self.views = views
        setupConstraints()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    @available(*, unavailable)
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setBorderColor(with color: UIColor) {
        stackView.getAllRows().forEach { (view) in
            view.subviews.forEach({ (view) in
                if let label = view as? UILabel {
                    label.textColor = color
                } else {
                    view.layer.borderColor = color.cgColor
                }
            })
        }
        
        privacySwitch.onTintColor = color
        termsAndConditionsSwitch.onTintColor = color
    }
    
    public func setupConstraints() {
        addSubview(stackView)
        stackView.edgesToSuperview()
        if !elements.isEmpty {
            for element in elements {
                let elementView = ProfileElementView(title: element, placeholder: element)
                stackView.addRow(elementView)
                elementView.height(60)
            }
        }
        
        if !views.isEmpty {
            for view in views {
                if let questionView = view as? SecretQuestionView {
                    stackView.addRow(questionView)
                    questionView.height(75)
                } else {
                    stackView.addRow(view)
                    view.height(60)
                }
            }
        }
        
        
        let privacyContainerView = UIView()
        privacyContainerView.stack([privacySwitch, privacyLabel], axis: .horizontal, spacing: 16)
        let termsAndConditionsContainerView = UIView()
        termsAndConditionsContainerView.stack([termsAndConditionsSwitch, termsAndConditionsLabel], axis: .horizontal, spacing: 16)
        
        let inset = UIEdgeInsets(top: 20, left: 40, bottom: 0, right: 40)
        let lastRows = [privacyContainerView, termsAndConditionsContainerView, saveButton]
        stackView.addRows(lastRows)
        stackView.setInset(forRow: saveButton, inset: inset)
        saveButton.height(50)
//        stackView.setInset(forRows: lastRows, inset: inset)
        let flexView = UIView()
        stackView.addRow(flexView)
        flexView.height(50)
    }
}

open class ProfileElementView: UIView, Constrainable {
    
    /// Set `.font` property for `UILabel` and `AdboxTextField`
    open var font: UIFont? {
        didSet {
            titleLabel.font = font
            textField.font = font
        }
    }
    
    public let titleLabel: UILabel = {
        let label = Template.Label(with: "Title")
        label.font = UIFont.systemFont(ofSize: 15)
        label.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        label.setHugging(.defaultHigh, for: .vertical)
        return label
    }()
    
    public let textField: AdboxTextField = {
        let textField = AdboxTextField()
        textField.layer.borderColor = UIColor.blue.cgColor
        textField.layer.borderWidth = 1.0
        textField.layer.cornerRadius = 5.0
        return textField
    }()
    
    public init(title: String, placeholder: String) {
        super.init(frame: .zero)
        titleLabel.text = title
        textField.placeholder = placeholder
        setupConstraints()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    @available(*, unavailable)
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupConstraints() {
        stack([titleLabel, textField], spacing: 8)
    }
}

open class AdboxTextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5)
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

public protocol SecretQuestionViewDelegate: class {
    func secretQuestionView(_ view: SecretQuestionView, didSelect question: String)
}

open class SecretQuestionView: UIView {
    
    public weak var delegate: SecretQuestionViewDelegate?
    
    open var font: UIFont {
        set {
            label.font = newValue
        }
        
        get {
            return label.font
        }
    }
    
    public lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.addTarget(self, action: #selector(didSelectPickerRow), for: .valueChanged)
        return datePicker
    }()
    
    public lazy var label: AdboxLabel = {
        let label = Template.createAdboxLabel(text: "--Selecciona tu pregunta--")
        label.textColor = tintColor
        label.setHugging(.defaultHigh, for: .vertical)
        return label
    }()
    
    public lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = tintColor
        return view
    }()
    
    public lazy var textField: AdboxTextField = {
        let textField = AdboxTextField()
        textField.layer.cornerRadius = 5.0
        textField.layer.borderColor = tintColor.cgColor
        textField.layer.borderWidth = 1.0
        textField.delegate = self
        return textField
    }()
    
    public var privateTextField: AdboxTextField = {
        let textField = AdboxTextField()
        return textField
    }()
    
    public convenience init() {
        self.init(frame: .zero)
        setupSubviews()
    }
    
    open override func tintColorDidChange() {
        super.tintColorDidChange()
        lineView.backgroundColor = tintColor
        textField.layer.borderColor = tintColor.cgColor
        label.textColor = tintColor
    }
    
    open func setupSubviews() {
        let view = UIView()
        addSubview(view)
        view.edgesToSuperview()
        lineView.height(1)
        view.stack([label, lineView, textField], axis: .vertical, spacing: 8)
        label.addSubview(privateTextField)
        privateTextField.edgesToSuperview()
        privateTextField.delegate = self
        label.isUserInteractionEnabled = true
    }
    
    @objc func didSelectPickerRow() {
        let date = datePicker.date
        label.text = "\(date)"
    }
}

extension SecretQuestionView: UITextFieldDelegate {
    open func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == privateTextField {
            textField.inputView = datePicker
        }
    }
    
    open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

