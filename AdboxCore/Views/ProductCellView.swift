//
//  ProductCellView.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/1/19.
//

import Foundation
import UIKit
import TinyConstraints

public protocol ProductCell {
    var productImageView: UIImageView { get }
    var footerView: GradientView { get }
    var titleLabel: UILabel { get }
    var pointsLabel: UILabel { get }
}

public class GradientView: UIView {

    public enum Axis: Int {
        case horizontal
        case vertical
    }

    public var axis: Axis = .horizontal

    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }

    public var gradientColors: [CGColor] = [] {
        didSet {
            guard let gradientLayer = layer as? CAGradientLayer else { return }
            if axis == .horizontal {
                gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
                gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            } else {
                gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
                gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
            }

            gradientLayer.colors = gradientColors
        }
    }
}

open class ProductCellView: UIView, ProductCell {
    public let productImageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    public let footerView: GradientView = {
        let view = GradientView()
        view.backgroundColor = .blue
        return view
    }()
    
    public let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .white
        return label
    }()
    
    public let pointsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .white
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, pointsLabel])
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    convenience public init() {
        self.init(frame: .zero)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupConstraints() {
        footerView.addSubview(stackView)
        stackView.edgesToSuperview()
        [productImageView, footerView].forEach { addSubview($0) }
        footerView.edgesToSuperview(excluding: .top)
        footerView.heightToSuperview(multiplier: 0.25)
        footerView.top(to: productImageView, productImageView.bottomAnchor)
        productImageView.edgesToSuperview(excluding: .bottom)
    }
    
}
