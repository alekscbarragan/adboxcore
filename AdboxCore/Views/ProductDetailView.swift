//
//  ProductDetailView.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/1/19.
//

import Foundation
import UIKit
import TinyConstraints
import AloeStackView

public protocol ProductDetail {
    var imageView: UIImageView { get }
    var titleLabel: UILabel { get }
    var categoryLabel: UILabel { get }
    var skuLabel: UILabel { get }
    var costLabel: UILabel { get }
    var descriptionLabel: UILabel { get }
    var addToCartButton: AdboxButton { get }
}

open class ProductDetailView: UIView, ProductDetail {
    public var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .red
        return imageView
    }()
    
    public var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.text = "Nombre producto"
        label.textColor = .black
        label.numberOfLines = 0
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    public var categoryLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.text = "Categoría: XXXXX"
        label.textColor = .black
        label.numberOfLines = 0
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    public var skuLabel: UILabel = {
        let label = UILabel()
        label.text = "SKU: XXXXX"
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    public var costLabel: UILabel = {
        let label = UILabel()
        label.text = "Points: XXXXX"
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()
    
    public var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.text = """
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        """
        label.textColor = .black
        label.numberOfLines = 0
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    public var addToCartButton: AdboxButton = {
        let button = AdboxButton()
        button.setTitle("Add to Cart", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .lightGray
        return button
    }()
    
    public let stackView: AloeStackView = {
        let stackView = AloeStackView()
        return stackView
    }()
    
    public lazy var internalStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [categoryLabel, skuLabel, costLabel])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    public convenience init() {
        self.init(frame: .zero)
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open func setupConstraints() {
        [imageView, stackView, addToCartButton].forEach { addSubview($0) }
//        stackView.rowInset = UIEdgeInsets(top: 4, left: 40, bottom: 4, right: 40)
        stackView.addRows([titleLabel, internalStackView, descriptionLabel])
        imageView.edgesToSuperview(excluding: .bottom)
        imageView.heightToSuperview(multiplier: 0.33)
        imageView.bottom(to: stackView, stackView.topAnchor, offset: 0)
        stackView.edgesToSuperview(excluding: [.top, .bottom])
        stackView.bottom(to: addToCartButton, addToCartButton.topAnchor)
        addToCartButton.edgesToSuperview(excluding: .top, insets: UIEdgeInsets(top: 0, left: 15, bottom: 20, right: 15))
        addToCartButton.height(50)
    }

    open func configure(image: UIImage?, name: String, price: String) {
        imageView.image = image
        titleLabel.text = name
        costLabel.text = price
    }
}
