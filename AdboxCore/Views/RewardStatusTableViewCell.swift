//
//  RewardStatusTableViewCell.swift
//  AdboxCore
//
//  Created by Alejandro Cárdenas on 1/15/19.
//

import UIKit

public class RewardStatusTableViewCell: UITableViewCell {
    
    public enum Status: Int {
        case processing, inTransit, delivered
    }
    
    public var status: Status = .processing {
        didSet {
            setStatus(status)
        }
    }
    
    var statusColors: [UIColor] = [.red, .yellow, .green]
    @IBOutlet public weak var statusImageView: UIImageView!
    
    @IBOutlet public weak var helpImageView: UIImageView!
    @IBOutlet public weak var infoContainerView: UIView!
    @IBOutlet public weak var statusLineView: UIView!
    @IBOutlet public weak var titleLabel: UILabel!
    @IBOutlet public weak var productImageView: UIImageView!
    @IBOutlet public weak var dateLabel: UILabel!
    @IBOutlet public weak var folioLabel: UILabel!
    @IBOutlet public weak var costLabel: UILabel!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }

    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func setStatus(_ status: Status?, color: UIColor? = nil) {
        guard let status = status else { return }
        switch status {
        case .processing:
            let color = color ?? UIColor.red
            infoContainerView.layer.borderColor = color.cgColor
            infoContainerView.layer.borderWidth = 1.0
            statusLineView.backgroundColor = color
            
        case .inTransit:
            let color = color ?? UIColor.yellow
            infoContainerView.layer.borderColor = color.cgColor
            infoContainerView.layer.borderWidth = 1.0
            statusLineView.backgroundColor = color
            
        case .delivered:
            let color = color ?? UIColor.red
            infoContainerView.layer.borderColor = color.cgColor
            infoContainerView.layer.borderWidth = 1.0
            statusLineView.backgroundColor = color
        }
    }
    
}
