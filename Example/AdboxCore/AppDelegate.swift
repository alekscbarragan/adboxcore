//
//  AppDelegate.swift
//  AdboxCore
//
//  Created by Aleks C. Barragan on 12/28/2018.
//  Copyright (c) 2018 Aleks C. Barragan. All rights reserved.
//

import UIKit
import AdboxCore



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let coordinator: Coordinator = {
        let navController = UINavigationController()
        let coordinator = Coordinator(with: navController)
        return coordinator
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = coordinator.navigationController
        window?.makeKeyAndVisible()
        coordinator.start()
        return true
    }
    
    func createRootViewController() -> UIViewController {
        let bundle = Bundle(for: CoreLoginViewController.self)
        let storyboard = UIStoryboard(name: "CoreLogin", bundle: bundle)
        return storyboard.instantiateInitialViewController() ?? UIViewController()
    }

}

