//
//  CartViewController.swift
//  AdboxCore_Example
//
//  Created by Alejandro Cárdenas on 1/7/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import AdboxCore

protocol CartViewControllerDelegate: class {
    func cartViewControllerDidConfirmExchange(_ viewController: CartViewController)
}

class CartViewController: UIViewController {
    
    lazy var dataSource: TableViewDataSource<String, CartTableViewCell> = {
        let dataSource = TableViewDataSource<String, CartTableViewCell>(
            withItems: items,
            cellIdentifier: CartTableViewCell.typedName,
            configureCell: { (cell, item) in
            
        })
        
        return dataSource
    }()
    
    var items = [String]()
    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        return tableView
    }()
    
    lazy var footerButton: UIButton = {
        let button = Template.Button(withTitle: "Confirmar Canje")
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .blue
        button.addTarget(self, action: #selector(confirmExchangeTouchUpInside(_:)), for: .touchUpInside)
        return button
    }()
    
    weak var delegate: CartViewControllerDelegate?
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 0...3 {
            items.append("Product \(i)")
        }
        
        view.addSubview(tableView)
        tableView.edgesToSuperview()
        tableView.register(CartTableViewCell.self, forCellReuseIdentifier: CartTableViewCell.typedName)
        tableView.dataSource = dataSource
        
        view.addSubview(footerButton)
        footerButton.edgesToSuperview(excluding: .top)
        footerButton.height(40)
        
    }
    
    @objc func confirmExchangeTouchUpInside(_ sender: UIButton) {
        print(#function)
        delegate?.cartViewControllerDidConfirmExchange(self)
    }

}

class CartTableViewCell: UITableViewCell {
    let view = CartCellView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(view)
        view.edgesToSuperview(insets: UIEdgeInsetsMake(8, 8, 8, 8))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
