//
//  CatalogViewController.swift
//  AdboxCore_Example
//
//  Created by Alejandro Cárdenas on 1/1/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import AdboxCore
import TinyConstraints

protocol CatalogViewControllerDelegate: class {
    func catalogViewControllerDidSelectCategory(_ viewController: CatalogViewController)
}

class CatalogViewController: UIViewController {
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return collectionView
    }()
    
    var items = [String]()
    
    weak var delegate: CatalogViewControllerDelegate?
    
    override func loadView() {
        view = collectionView
        collectionView.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 0...2 {
            items.append("Category \(i)")
        }
        
        collectionView.register(CatalogCollectionViewCell.self, forCellWithReuseIdentifier: CatalogCollectionViewCell.typedName)
        collectionView.dataSource = self
        collectionView.delegate = self
    }

}

extension CatalogViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let val = indexPath.row % 3
        let isTwoColumns = val == 0 ? false : true
        let size: CGSize
        
        if isTwoColumns {
            let padding: CGFloat = 30.0
            let totalWidth = (collectionView.frame.width - CGFloat(padding)) / 2
            size = CGSize(width: totalWidth, height: 250)
        } else {
            let padding: CGFloat = 10.0
            let width = collectionView.frame.width - (padding * 2)
            size = CGSize(width: width, height: 250)
        }
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)
    }
}

extension CatalogViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.catalogViewControllerDidSelectCategory(self)
    }
}

extension CatalogViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CatalogCollectionViewCell.typedName, for: indexPath) as? CatalogCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.contentView.backgroundColor = .red
        cell.view.categoryLabel.text = items[indexPath.row]
        
        return cell
    }
    
}

class CatalogCollectionViewCell: UICollectionViewCell {
    
    let view = CatalogCellView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        contentView.addSubview(view)
        view.edgesToSuperview()
    }
}
