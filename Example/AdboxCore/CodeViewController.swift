//
//  CodeViewController.swift
//  AdboxCore_Example
//
//  Created by Alejandro Cárdenas on 1/7/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import AdboxCore

protocol CodeViewControllerDelegate: class {
    func codeViewControllerDidAccept(_ viewController: CodeViewController)
}

class CodeViewController: UIViewController {

    
    weak var delegate: CodeViewControllerDelegate?

    let codeAlertView: CodeAlertView = {
        let view = CodeAlertView(header: "CÓDIGO DE PEDIDO", title: "¡Gracias por realizar el canje! Tu pedido está en proceso", subtitle: "El número de folio para tu pedido es:", code: "H8UL9")
        return view
    }()
    
    override func loadView() {
        view = codeAlertView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        codeAlertView.acceptButton.addTarget(self, action: #selector(acceptButtonTouchUpInside(_:)), for: .touchUpInside)
    }

    
    @objc func acceptButtonTouchUpInside(_ sender: UIButton) {
        delegate?.codeViewControllerDidAccept(self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
