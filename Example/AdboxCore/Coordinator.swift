//
//  Coordinator.swift
//  AdboxCore_Example
//
//  Created by Alejandro Cárdenas on 1/7/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import AdboxCore

class Container {
    let centerVC: UINavigationController
    init(with centerVC: UINavigationController) {
        self.centerVC = centerVC
    }
    
    func showMenuOver() {
        var frame = centerVC.view.bounds
        frame.size.width *= 0.6
        let view = UIView(frame: frame)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        centerVC.view.addSubview(view)
    }
}

class Coordinator: BaseCoordinator {
    
    let container: Container
    
    let navigationController: UINavigationController
    
    init(with navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.container = Container(with: navigationController)
    }
    
    func start() {
        let logInViewController = CoreLoginViewController()
        logInViewController.delegate = self
        navigationController.pushViewController(logInViewController, animated: false)
    }
    
    func toggleMenu() {
        container.showMenuOver()
    }
    
}

extension Coordinator: SideMenuShowable {
    func showSideMenu() {
        container.showMenuOver()
    }
}

extension Coordinator: CoreLoginViewControllerDelegate {
    func viewControllerDidLogInSuccessfully(_ viewController: CoreLoginViewController) {
        // Home
        let vc = HomeViewController()
        vc.delegate = self
        navigationController.show(vc, sender: viewController)
    }
    
    func logInViewControllerDidSelectForgotPassword(_ viewController: CoreLoginViewController) {
        let vc = ForgotPasswordViewController()
        navigationController.show(vc, sender: self)
    }
}

extension Coordinator: HomeViewControllerDelegate {
    func homeViewControllerDidSelectOption(_ viewController: HomeViewController) {
        // Catalog
        let vc = CatalogViewController()
        vc.delegate = self
        navigationController.show(vc, sender: viewController)
    }
}

extension Coordinator: CatalogViewControllerDelegate {
    func catalogViewControllerDidSelectCategory(_ viewController: CatalogViewController) {
        // Products
        let vc = ProductsViewController()
        vc.delegate = self
        navigationController.show(vc, sender: viewController)
    }
}

extension Coordinator: ProductsViewControllerDelegate {
    func productsViewControllerDidSelectProduct(_ viewController: ProductsViewController) {
        // Product detail
        let vc = ProductDetailViewController()
        vc.delegate = self
        navigationController.show(vc, sender: viewController)
    }
}

extension Coordinator: ProductDetailViewControllerDelegate {
    func productDetailViewControllerDidAddToCart(_ viewController: ProductDetailViewController) {
        let vc = CartViewController()
        vc.delegate = self
        navigationController.show(vc, sender: viewController)
    }
}

extension Coordinator: CartViewControllerDelegate {
    func cartViewControllerDidConfirmExchange(_ viewController: CartViewController) {
        let exchangeDetailViewController = ExchangeDetailViewController()
        exchangeDetailViewController.delegate = self
        navigationController.show(exchangeDetailViewController, sender: viewController)
    }
}

extension Coordinator: ExchangeDetailViewControllerDelegate {
    func exchangeDetailViewControllerDidConfirmExchange(_ viewController: ExchangeDetailViewController) {
        let codeViewController = CodeViewController()
        codeViewController.modalPresentationStyle = .overCurrentContext
        codeViewController.delegate = self
        navigationController.present(codeViewController, animated: true)
    }
}

extension Coordinator: CodeViewControllerDelegate {
    func codeViewControllerDidAccept(_ viewController: CodeViewController) {
        print(#function)
        viewController.dismiss(animated: true) {
            if let catalogViewController = self.navigationController.viewControllers.first(where: { $0 is CatalogViewController }) {
                self.navigationController.popToViewController(catalogViewController, animated: true)
            }
        }
    }
}
