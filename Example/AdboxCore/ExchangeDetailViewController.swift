//
//  ExchangeDetailViewController.swift
//  AdboxCore_Example
//
//  Created by Alejandro Cárdenas on 1/7/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import AloeStackView
import AdboxCore

protocol ExchangeDetailViewControllerDelegate: class {
    func exchangeDetailViewControllerDidConfirmExchange(_ viewController: ExchangeDetailViewController)
}

class ExchangeDetailViewController: UIViewController {
    
    weak var delegate: ExchangeDetailViewControllerDelegate?
    let stackView = AloeStackView()
    
    lazy var footerButton: UIButton = {
        let button = Template.Button(withTitle: "Confirmar Canje")
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .blue
        button.addTarget(self, action: #selector(confirmExchangeTouchUpInside(_:)), for: .touchUpInside)
        return button
    }()
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
        view.addSubview(stackView)
        stackView.edgesToSuperview()
        
        view.addSubview(footerButton)
        footerButton.edgesToSuperview(excluding: .top)
        footerButton.height(40)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let total = ExchangeDetailCellView(title: "Importe Total", subtitle: "1,600")
        let available = ExchangeDetailCellView(title: "Puntos Disponibles", subtitle: "9,999")
        let after = ExchangeDetailCellView(title: "Puntos después de la compra", subtitle: "8,399")
        [total, available, after].forEach { stackView.addRow($0) }
        
    }
    
    @objc func confirmExchangeTouchUpInside(_ sender: UIButton) {
        print(#function)
        delegate?.exchangeDetailViewControllerDidConfirmExchange(self)
    }

}
