//
//  ForgotPasswordViewController.swift
//  AdboxCore_Example
//
//  Created by Alejandro Cárdenas on 1/10/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import AdboxCore

class ForgotPasswordViewController: CoreForgotPasswordViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func emailButtonTouchUpInside(_ sender: UIButton) {
        print("ForgotPasswordViewController")
    }
    
    override func faqButtonTouchUpInside(_ sender: UIButton) {
        print("ForgotPasswordViewController")
    }

}
