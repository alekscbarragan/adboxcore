//
//  HomeViewController.swift
//  AdboxCore_Example
//
//  Created by Alejandro Cárdenas on 12/31/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import AloeStackView
import AdboxCore

protocol HomeViewControllerDelegate: class {
    func homeViewControllerDidSelectOption(_ viewController: HomeViewController)
}

class HomeViewController: AloeStackViewController {
    
    weak var delegate: HomeViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        stackView.rowInset = UIEdgeInsets(top: 4, left: 20, bottom: 4, right: 20)
        stackView.hidesSeparatorsByDefault = true
        let catalog = HomeCellView()
        [catalog].forEach { (view) in
            view.height(100)
            stackView.addRow(view)
        }
        
        stackView.setTapHandler(forRow: catalog) { [weak self] (rowView) in
            guard let self = self else { return }
            self.delegate?.homeViewControllerDidSelectOption(self)
        }
        
    }
    
}
