//
//  ProductDetailViewController.swift
//  AdboxCore_Example
//
//  Created by Alejandro Cárdenas on 1/1/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import AdboxCore
import TinyConstraints

protocol ProductDetailViewControllerDelegate: class {
    func productDetailViewControllerDidAddToCart(_ viewController: ProductDetailViewController)
}

class ProductDetailViewController: UIViewController {
    
    let detailView: ProductDetailView = {
        let view = ProductDetailView()
        return view
    }()
    
    weak var delegate: ProductDetailViewControllerDelegate?
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
        view.addSubview(detailView)
        detailView.edgesToSuperview()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        detailView.addToCartButton.addTarget(self, action: #selector(addToCart), for: .touchUpInside)
    }
    
    @objc func addToCart() {
        delegate?.productDetailViewControllerDidAddToCart(self)
    }
    
}
