//
//  ProductsViewController.swift
//  AdboxCore_Example
//
//  Created by Alejandro Cárdenas on 1/1/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import TinyConstraints
import AdboxCore

protocol ProductsViewControllerDelegate: class {
    func productsViewControllerDidSelectProduct(_ viewController: ProductsViewController)
}

class ProductsViewController: UIViewController {
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return collectionView
    }()
    weak var delegate: ProductsViewControllerDelegate?
    
    var dataSource: CollectionViewDataSource<ProductCollectionViewCell, String>!
    var items = ["Product 1", "Product 2", "Product 3", "Product 4", "Product 5", "Product 6"]

    lazy var source: CollectionViewDataSource<ProductCollectionViewCell, String> = {
        let dataSource = CollectionViewDataSource<ProductCollectionViewCell, String>(
            with: items,
            cellIdentifier: ProductCollectionViewCell.typedName,
            configureCell: configureCell)

        return dataSource
    }()

    func configureCell(_ cell: ProductCollectionViewCell, item: String) {
        cell.view.productImageView.backgroundColor = .red
        cell.view.titleLabel.text = item
        cell.view.pointsLabel.text = item
    }
    
    override func loadView() {
        view = collectionView
        collectionView.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = CollectionViewDataSource<ProductCollectionViewCell, String>(
            with: ["Product 1", "Product 2", "Product 3", "Product 4", "Product 5", "Product 6"],
            cellIdentifier: ProductCollectionViewCell.typedName,
            configureCell: { (cell, item) in
                cell.view.productImageView.backgroundColor = .red
                cell.view.titleLabel.text = item
                cell.view.pointsLabel.text = item
        })
        
        collectionView.register(ProductCollectionViewCell.self, forCellWithReuseIdentifier: ProductCollectionViewCell.typedName)
        collectionView.dataSource = source
        collectionView.delegate = self
    }
    
}

extension ProductsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 7.5
        let width = (collectionView.frame.width / 2) - (padding * 2)
        return CGSize(width: width, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)
    }
}

extension ProductsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(#function)
        delegate?.productsViewControllerDidSelectProduct(self)
    }
}

class ProductCollectionViewCell: UICollectionViewCell {
    
    let view = ProductCellView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupConstraints() {
        contentView.addSubview(view)
        view.edgesToSuperview()
    }
    
}
