//
//  ViewController.swift
//  AdboxCore
//
//  Created by Aleks C. Barragan on 12/28/2018.
//  Copyright (c) 2018 Aleks C. Barragan. All rights reserved.
//

import UIKit
import AdboxCore
import TinyConstraints

class ViewController: UIViewController {
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let label = UILabel()
        label.text = "View Controller"
        label.textAlignment = .center
        label.textColor = .black
        view.addSubview(label)
        label.edgesToSuperview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
