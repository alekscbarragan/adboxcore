//
//  Resource.swift
//  AdboxNetworkLib
//
//  Created by Alejandro Cárdenas on 12/27/18.
//

import Foundation

struct Resource<A> {
    let url: URL
    let parse: (Data) -> A?
}
