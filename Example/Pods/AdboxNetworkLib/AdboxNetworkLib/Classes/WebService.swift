//
//  WebService.swift
//  AdboxNetworkLib
//
//  Created by Alejandro Cárdenas on 12/27/18.
//

import Foundation

final class WebService {
    func load<A>(resource: Resource<A>, completion: @escaping (A?) -> Void) {
        URLSession.shared.dataTask(with: resource.url) { (data, _, _) in
            guard let data = data else {
                completion(nil)
                return
            }
            completion(resource.parse(data))
            }.resume()
    }
}
