# AdboxNetworkLib

[![CI Status](https://img.shields.io/travis/izhuster/AdboxNetworkLib.svg?style=flat)](https://travis-ci.org/izhuster/AdboxNetworkLib)
[![Version](https://img.shields.io/cocoapods/v/AdboxNetworkLib.svg?style=flat)](https://cocoapods.org/pods/AdboxNetworkLib)
[![License](https://img.shields.io/cocoapods/l/AdboxNetworkLib.svg?style=flat)](https://cocoapods.org/pods/AdboxNetworkLib)
[![Platform](https://img.shields.io/cocoapods/p/AdboxNetworkLib.svg?style=flat)](https://cocoapods.org/pods/AdboxNetworkLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AdboxNetworkLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AdboxNetworkLib'
```

## Author

izhuster, leks.bar@icloud.com

## License

AdboxNetworkLib is available under the MIT license. See the LICENSE file for more info.
