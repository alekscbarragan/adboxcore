//
//  String+FieldValidations.swift
//  AdboxUserInputValidations
//
//  Created by Alejandro Cárdenas on 12/27/18.
//

import Foundation

public extension String {

    /// Checks if the `String` instance is a valid email.
    var isEmail: Bool {
        return matches(regex: RegexUtilities.email.rawValue)
    }

    // MARK: - Regex Helper
    private func matches(regex: String) -> Bool {
        let predicateTest = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicateTest.evaluate(with: self)
    }
}
