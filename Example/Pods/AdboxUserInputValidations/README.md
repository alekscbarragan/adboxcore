# AdboxUserInputValidations

[![CI Status](https://img.shields.io/travis/izhuster/AdboxUserInputValidations.svg?style=flat)](https://travis-ci.org/izhuster/AdboxUserInputValidations)
[![Version](https://img.shields.io/cocoapods/v/AdboxUserInputValidations.svg?style=flat)](https://cocoapods.org/pods/AdboxUserInputValidations)
[![License](https://img.shields.io/cocoapods/l/AdboxUserInputValidations.svg?style=flat)](https://cocoapods.org/pods/AdboxUserInputValidations)
[![Platform](https://img.shields.io/cocoapods/p/AdboxUserInputValidations.svg?style=flat)](https://cocoapods.org/pods/AdboxUserInputValidations)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AdboxUserInputValidations is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AdboxUserInputValidations'
```

## Author

izhuster, leks.bar@icloud.com

## License

AdboxUserInputValidations is available under the MIT license. See the LICENSE file for more info.
