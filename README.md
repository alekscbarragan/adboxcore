# AdboxCore

[![CI Status](https://img.shields.io/travis/Aleks C. Barragan/AdboxCore.svg?style=flat)](https://travis-ci.org/Aleks C. Barragan/AdboxCore)
[![Version](https://img.shields.io/cocoapods/v/AdboxCore.svg?style=flat)](https://cocoapods.org/pods/AdboxCore)
[![License](https://img.shields.io/cocoapods/l/AdboxCore.svg?style=flat)](https://cocoapods.org/pods/AdboxCore)
[![Platform](https://img.shields.io/cocoapods/p/AdboxCore.svg?style=flat)](https://cocoapods.org/pods/AdboxCore)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

AdboxCore is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'AdboxCore'
```

## Author

Aleks C. Barragan, leks.bar@icloud.com

## License

AdboxCore is available under the MIT license. See the LICENSE file for more info.
